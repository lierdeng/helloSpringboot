package com.ding.learn.test;

public class Demo {
    static {
        System.out.println("静态代码块 111");
    }
    static {
        System.out.println("静态代码块 222");
    }
    {
        System.out.println("初始块 111");
    }
    {
        System.out.println("初始块 222");
    }
    public Demo() {
        System.out.println("无参构造方法");
    }
    public Demo(int i) {
        System.out.println("有参构造方法" + i);
    }
    public static void main(String[] args) {
        new Demo();
    }
}
