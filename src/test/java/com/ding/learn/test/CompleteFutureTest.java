package com.ding.learn.test;

import org.junit.Test;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CompleteFutureTest {

    @Test
    public void test1() {
        List<MyTask> tasks = IntStream.range(0, 10)
                .mapToObj(i -> new MyTask(1))
                .collect(Collectors.toList());

        long start = System.nanoTime();
        List<Integer> result = tasks.parallelStream()
                .map(MyTask::calculate)
                .collect(Collectors.toList());
        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.printf("Processed %d tasks in %d millis\n", tasks.size(), duration);
        System.out.println(result);
    }

    @Test
    /**
     * CompletableFutures可以更多的控制线程池的数量。如果你的任务是io密集型的，
     * 你应该使用CompletableFutures；否则如果你的任务是cpu密集型的，使用比处理器更多的线程是没有意义的，
     * 所以选择parallel stream，因为它更容易使用.
     */
    public void test2() {
        long start = System.nanoTime();
        //1、parallelstream并行流方式
        /*IntStream.range(0, 10).boxed().parallel().map(n-> {
            try {
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(1000);
                return 1;
            } catch (final InterruptedException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());*/

        //2、CompletableFutures写法
        List<CompletableFuture<Integer>> futures = IntStream.range(0, 10).boxed().map(n-> CompletableFuture.supplyAsync(()->{
            try {
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(1000);
                return n;
            } catch (final InterruptedException e) {
                throw new RuntimeException(e);
            }
        })).collect(Collectors.toList());

        List<Integer> result =
                futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList());

        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.printf("Processed tasks in %d millis\n", duration);
//        System.out.println(result);
    }
}


class MyTask {
    private final int duration;
    public MyTask(int duration) {
        this.duration = duration;
    }
    public int calculate() {
        System.out.println(Thread.currentThread().getName());
        try {
            Thread.sleep(duration * 1000);
        } catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }
        return duration;
    }
}