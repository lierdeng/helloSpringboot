package com.ding.learn.test;

import com.alibaba.fastjson.JSONObject;
import com.ding.learn.test.dto.Stu;
import com.ding.learn.test.dto.Student;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.mockito.stubbing.Stubber;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Test1 {

    @Test
    public void test() {
        Integer type = 2;
        type = Optional.ofNullable(type).orElse(1);
        Integer.valueOf(null);
        System.out.printf(type+"");
    }

    @Test
    public void test1(){
        String deliveryIds = "2223232, 2323";
        boolean result = Pattern.matches("^(\\d+,)*\\d+$",deliveryIds);
        System.out.println(result);
        System.out.println(Lists.newArrayList("12324".split(",")));
        System.out.println(Integer.parseInt("2147483641"));
    }

    @Test
    public void test2() {
        List<String> list =  Lists.newArrayList("12324,333,666,7777".split(","));
        double aa = 0;
        System.out.println(aa);
        System.out.println(getList(list));
    }

    private String getList(List<String> list) {
        list.forEach(n-> {
            if("333".equals(n)) {
                return;
            }
            System.out.println("1232");
        });
//        for (String delivery: list) {
//            if("333".equals(delivery)) {
//                return "结束";
//            }
//            System.out.println("1232");
//        }
        /*for (int i=0; i<list.size();i++) {
            if("333".equals(list.get(i))) {
                return "结束";
            }
            System.out.println("1232");
        }*/
        return null;
    }

    @Test
    public void test3() {
        String note = "热送贴,Pickup";
        System.out.println(note.toUpperCase(Locale.ROOT));
        System.out.println(note.contains("贴，Pickup"));
        System.out.println(new JSONObject().toJSONString());
    }

    @Test
    public void test4() {
        String note = getName();
        System.out.println(note);
    }

    private String  getName() {
        int i = 0;
        try {
            i = 10/2;
            return "ding";
        } catch (Exception e) {
            System.out.println("exception");
        } finally {
            System.out.println("log info8888888888888   " + i);
        }
        return "111";
    }

    @Test
    public void test5() {
        System.out.println(StringUtils.leftPad(String.valueOf(5), 4, "0"));
        JSONObject json = new JSONObject(new LinkedHashMap());
        json.put("dry_box_count",1);
        json.put("not_dry_box_count", 2);
        json.put("special_quantity", "0");
        json.put("has_aclohol", "Y");
        System.out.println(json);
        System.out.println(json.toJSONString());
        System.out.println(JSONObject.toJSONString(json));
    }

    @Test
    public void test6() {
//        final TimeZone timeZone = TimeZone.getTimeZone(ZoneId.of("GMT+06:00"));
//        final TimeZone timeZone = TimeZone.getTimeZone("UTC");
//        TimeZone.setDefault(timeZone);
        System.out.println(TimeZone.getDefault());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String eta =  "2022-01-20T00:00:00Z";
//        String eta =  "2022-01-25T20:30:00Z";
        try {
            Date estDate = sdf.parse(eta);
            System.out.println(estDate);
            System.out.println(estDate.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test7() {
        final TimeZone timeZone = TimeZone.getTimeZone("UTC");
        TimeZone.setDefault(timeZone);
        System.out.println(new Date());
        System.out.println(TimeZone.getDefault());
        Date data = new Date(1644272248000L);
        System.out.println(data);
    }

    @Test
    public void test8() {
        Lists.newArrayList("1","3","5").stream().forEach(n-> {
            if("3".equals(n)) {
               return;
            }
            System.out.println(n);
        });
    }

    @Test
    public void test9() {
        Student s1 = new Student(1, "zhangsan", "111");
        Student s2 = new Student(2, "xiaohong", "222");
        Student s3 = new Student(1, "zhangsan", "333");
        Student s4 = new Student(2, "xiaohong", "444");

        List<Student> list = Lists.newArrayList();
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);

        List<Student> curStudent =  list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(
                () -> new TreeSet<>(Comparator.comparing(o -> o.getId() + "#" + o.getName()))), ArrayList::new));

        curStudent.stream().forEach(System.out::println);

        int a = 1;
        System.out.println();


        System.out.println("test cheek pick");
    }

    @Test
    public void test10() {
        for (int i = 0; i < 10; i++) {
            if(i == 2) {
                return;
            }
            System.out.println(i);
        }
        System.out.println("wewew");
    }
}
