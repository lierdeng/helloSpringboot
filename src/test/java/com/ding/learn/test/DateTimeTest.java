package com.ding.learn.test;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DateTimeTest {
    @Test
    public void test1() {
        // 随便虚拟一个日期
        LocalDate now = LocalDate.now().minusDays(3);
        System.out.println("当前日期: " + now + " " + now.getDayOfWeek().getValue());
        System.out.println(now.plusDays(1));
        // 求这个日期上一周的周一、周日
        LocalDate todayOfLastWeek = now.plusDays(7);
        LocalDate monday = todayOfLastWeek.with(TemporalAdjusters.previous(DayOfWeek.SUNDAY)).plusDays(1);
        LocalDate sunday = todayOfLastWeek.with(TemporalAdjusters.next(DayOfWeek.MONDAY)).minusDays(1);
        System.out.println("当前日期：" + now + " 上一周的周一：" + monday + " " + monday.getDayOfWeek());
        System.out.println("当前日期：" + now + " 上一周的周日：" + sunday + " " + sunday.getDayOfWeek());

    }

    @Test
    public void test2() throws Exception {
        System.out.println("0 kaishi-------");
//        CountDownLatch countDownLatch = new CountDownLatch(5);
        ExecutorService tpe = Executors.newSingleThreadExecutor();
        tpe.execute(()-> {
            System.out.println(Thread.currentThread().getName() + " testtest");
            for (int i = 0; i<5;i++) {
                try {
//                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName() + "dsdsdsdsd ");
//                    countDownLatch.countDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
//        countDownLatch.countDown();
//        countDownLatch.countDown();
//        countDownLatch.countDown();
//        countDownLatch.countDown();
//        countDownLatch.countDown();
        System.out.println(Thread.currentThread().getName() + " main thread await. ");
//        countDownLatch.await();
        System.out.println(Thread.currentThread().getName() + " start");
//        countDownLatch.countDown();
        System.out.println(Thread.currentThread().getName() + " end");
    }
    @Test
    public void test3(){
        double a = 0.0;
        if(a != 0) {
            System.out.println("111");
        }
    }

    @Test
    public void  test4() {
        DateTimeFormatter curDtf = DateTimeFormatter.ofPattern("M/d/yyyy h:m:s a", Locale.ENGLISH);
        String  curDateStr = "2/7/2022 5:43:51 AM";
        LocalDateTime curDateTime =  LocalDateTime.parse(curDateStr,curDtf);
        System.out.println(Arrays.toString("zhang,san,de".split(",")));
        System.out.println(curDateTime.toInstant(ZoneOffset.UTC).toString());
    }

    @Test
    public void test5() {
        String values = "";
        List<String> list = Arrays.stream(values.split(",")).map(n->n.trim()).collect(Collectors.toList());
        if(CollectionUtils.isEmpty(list)) {
            System.out.println("111111");
        }
        System.out.println(list);
    }

    @Test
    public void test6() {
        System.out.println(System.currentTimeMillis()/1000);
        DateTimeFormatter curDtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX");
        String  curDateStr = "2022-03-21T16:25:12-07:00";
        OffsetDateTime curDateTime =  OffsetDateTime.parse(curDateStr,curDtf);
        System.out.println(curDateTime.toInstant().toString());
        System.out.println(curDateTime.toEpochSecond());
    }

    @Test
    public void test7() {
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime.toInstant(ZoneOffset.UTC).toString());
    }

    @Test
    public void test8() {
        Long time = Instant.now().getEpochSecond();
        DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String  result1 =  LocalDateTime.ofInstant(Instant.ofEpochSecond(time),ZoneOffset.UTC).format(ftf);
       String  result =  ftf.format(LocalDateTime.ofInstant(Instant.ofEpochSecond(time),ZoneOffset.UTC));
        System.out.println(result1);
        System.out.println(result);
    }

    @Test
    public void test9() {
        System.out.println(Math.ceil(1.000000000001));
        System.out.println(parseUTCMill("2022-04-14T00:00:00-06:00"));
        System.out.println(parseUTCString("2022-04-14T00:00:00-06:00"));
    }

    @Test
    public void test10() {
        Double ss = 1.0 / 0;
        System.out.println(ss);
        System.out.println(ss.doubleValue());

        double c = Double.NaN;
        if ((0 > c) || (0 == c) || (0 < c) || (c == Double.NaN)) {
            System.out.println("NaN compared with 0 is not always false.");
        } else {
            //执行这个
            System.out.println("NaN compared with anything is always false!");
        }
    }

    private static String parseUTCString(String curDateStr) {
        String result = curDateStr;
        if(StringUtils.isEmpty(curDateStr)) {
            return result;
        }
        if(curDateStr.length() > 19) {
            DateTimeFormatter curDtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX");
            OffsetDateTime curDateTime =  OffsetDateTime.parse(curDateStr,curDtf);
            result = curDateTime.toInstant().toString();
        } else {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss",Locale.ENGLISH);
            LocalDateTime curDateTime = LocalDateTime.parse(curDateStr, dtf);
            result = curDateTime.toInstant(ZoneOffset.UTC).toString();
        }
        return result;
    }

    private static Long parseUTCMill(String curDateStr) {
        Long result = System.currentTimeMillis() / 1000;
        if(StringUtils.isEmpty(curDateStr)) {
            return result;
        }
        if(curDateStr.length() > 19) {
            DateTimeFormatter curDtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX");
            OffsetDateTime curDateTime =  OffsetDateTime.parse(curDateStr,curDtf);
            result = curDateTime.toEpochSecond();
        } else {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss",Locale.ENGLISH);
            LocalDateTime curDateTime = LocalDateTime.parse(curDateStr, dtf);
            result = curDateTime.toEpochSecond(ZoneOffset.UTC);
        }
        return result;
    }
}
