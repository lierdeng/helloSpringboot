package com.ding.learn.config.hash;

import java.io.Serializable;

public interface Shape extends Serializable {
    Float getArea();
}