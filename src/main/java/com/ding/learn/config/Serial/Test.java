package com.ding.learn.config.Serial;

import java.io.*;
import java.util.HashMap;

/**
 *测试变量声明关键字：transient的用法
 * 含义：将不需要序列化的属性前添加关键字transient，序列化对象的时候，这个属性就不会被序列化
 */
public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        serial();
//        deSerial();
        User user = new User();
        User.putVal("eeeee");
        System.out.println(user.getXing());
        int a = User.DEFAULT_INITIAL_CAPACITY;
    }

    private static void serial() throws IOException {
        User user = new User();
        user.setAge(32);
        user.setName("张飞");
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D://Test/serial"));
        oos.writeObject(user);
        oos.close();
        System.out.println("添加序列化name："+user.getName()+" age:" + user.getAge());
    }

    private static void deSerial() throws IOException, ClassNotFoundException {
        File file = new File("D://Test/serial");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
        User user = (User) ois.readObject();
        System.out.println("添加反序列化：name："+user.getName()+" age:" + user.getAge());
    }
}
