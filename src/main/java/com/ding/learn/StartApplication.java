package com.ding.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
//@EnableScheduling
@EnableAsync
public class StartApplication {

	@Bean
	public RestTemplate restTemplate() {return  new RestTemplate();}
	//0512
	public static void main(String[] args) {
		SpringApplication.run(StartApplication.class, args);
	}
} 
