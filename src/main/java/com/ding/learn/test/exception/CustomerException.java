package com.ding.learn.test.exception;

public class CustomerException extends RuntimeException{

    public CustomerException(String message) {
        super(message);
    }
}
