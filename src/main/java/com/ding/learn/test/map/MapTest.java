package com.ding.learn.test.map;

import java.util.HashMap;
import java.util.Map;

public class MapTest {
    public static void main(String[] args) {
        Map<Object, Object> map = new HashMap<>();
        map.put("name", "RebornChang");
        map.put("gender", "男");
        map.put("phone", "18888888888");
        map.put(null, null);
        //1.Map的迭代
        // 通用的Map迭代方式
        System.out.println("==============Map的迭代======================");
        for (Map.Entry<Object, Object> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "：" + entry.getValue());
        }
        System.out.println("====================================");
        // JDK8的迭代方式
        map.forEach((key, value) -> {
            System.out.println(key + "：" + value);
        });
    }
}
