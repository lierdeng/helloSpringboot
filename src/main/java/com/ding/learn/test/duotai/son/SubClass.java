package com.ding.learn.test.duotai.son;

import com.ding.learn.test.duotai.SuperClass;

class SubClass extends SuperClass {
  private int n;
  
  SubClass(){ // 自动调用父类的无参数构造器
    System.out.println("SubClass");
  }  
  
  public SubClass(int n){ 
    super(300);  // 调用父类中带有参数的构造器
    System.out.println("SubClass(int n):"+n);
    this.n = n;
  }
}