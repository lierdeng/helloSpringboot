package com.ding.learn.test.duotai.son;

import com.ding.learn.test.duotai.Animal;

public class Penguin extends Animal {

    public Penguin(String myName, int myid) {
        super(myName, myid);
    }

    @Override
    public void sleep() {
        super.sleep();
    }


    public void eat(String name) {
        System.out.println("正在吃s");
    }

    public static void main(String[] args) {
        Penguin penguin = new Penguin("磅礴",3);
        penguin.eat();
        penguin.sleep();
        penguin.introduction();
    }
}
