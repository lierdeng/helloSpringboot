package com.ding.learn.test.digui;

public class Factorial {
    int fact(int n) {
        int result;
        if (n == 1) {
            System.out.println("fact:" + n + ":"+ 1);
            return 1;
        }
        result = fact(n - 1) * n;
        System.out.println("fact:" + n + ":"+ result);
        return result;
    }
}
class Recursion {
    public static void main(String args[]) {
        Factorial f = new Factorial();
        System.out.println("3的阶乘是 " + f.fact(3));
//        System.out.println("4的阶乘是 " + f.fact(4));
//        System.out.println("5的阶乘是 " + f.fact(5));
    }
}