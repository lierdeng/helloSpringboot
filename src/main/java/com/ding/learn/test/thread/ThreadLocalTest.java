package com.ding.learn.test.thread;

public class ThreadLocalTest {
    public static void main(String[] args) {
        ThreadLocal<String> threadLocal1 = new ThreadLocal<>();
        threadLocal1.set("mayi");
        threadLocal1 = null;
        Thread thread = Thread.currentThread();
        ThreadLocal<String> threadLocal2 = new ThreadLocal<>();
        threadLocal2.set("zhangl");
        threadLocal2.remove();

        System.out.println(threadLocal1.get());
        System.out.println(threadLocal2.get());
    }
}
