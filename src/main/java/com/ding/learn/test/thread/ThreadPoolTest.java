package com.ding.learn.test.thread;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

public class ThreadPoolTest {
    public static LinkedBlockingDeque runableDeque;
    public static void main1(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        for (int i=0;i<10;i++) {
            final int finalI = i;
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName()+","+finalI);
                }
            });
        }
//        executorService.shutdown();
    }


    public static void main(String[] args) {
        runableDeque = new LinkedBlockingDeque<Runnable>(5);
        runableDeque.add("zhangsan");
        runableDeque.add("lisi");
        runableDeque.add("wangwu");
        new MyThread().start();
    }
    static class MyThread extends Thread {
        @Override
        public void run() {
            while (true) {
                System.out.println(runableDeque.poll()+","+Thread.currentThread().getName()+","+Thread.currentThread().getId());
            }
        }


    }
}
