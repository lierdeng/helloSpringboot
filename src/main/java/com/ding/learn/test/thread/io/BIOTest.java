package com.ding.learn.test.thread.io;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;

public class BIOTest {
    public static void main(String[] args) throws IOException {
        ServerSocket socket = new ServerSocket(9123);
       //nio ServerSocketChannel.open();
        while (true) {
            System.out.println("等待连接.....");
            //阻塞方法
            Socket clientSocket =  socket.accept();
            System.out.println("有客户端接入连接:"+ clientSocket.getRemoteSocketAddress());
            handle(clientSocket);
        }

    }

    private static void handle(Socket clientSocket) throws IOException {
        byte[] bytes = new byte[1024];
        System.out.println("准备 read...");
        //读取数据，阻塞方法
        int read = clientSocket.getInputStream().read(bytes);
        System.out.println("read完毕...");
        if(read != -1) {
            System.out.println(new String(bytes,0,read));
        }


    }
}
