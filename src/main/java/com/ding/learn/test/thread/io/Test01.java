package com.ding.learn.test.thread.io;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;

public class Test01 {
    private static int a,b = 0;
    private static int x,y = 0;

    public static void main1(String[] args) throws InterruptedException {
        Set<String> result = new HashSet<String>();
        for (int i=0;i<10000;i++) {
            Thread one = new Thread(()->{
                a = y;
                x = 1;
            });
            Thread other = new Thread(()->{
                b = x;
                y = 1;
            });
            one.start();
            other.start();
            one.join();
            other.join();

            result.add("a="+a+","+ "b="+ b);
            System.out.println(result);
        }
    }

    public static void main(String[] args)
    {
        BigDecimal a = new BigDecimal("4.0");

        a = a.setScale(3, RoundingMode.HALF_UP); //保留3位小数，且四舍五入
        System.out.println(a.toString());
    }
}
