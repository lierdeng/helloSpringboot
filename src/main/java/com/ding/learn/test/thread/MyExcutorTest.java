package com.ding.learn.test.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

public class MyExcutorTest {
   private List<WorkThread> workList;
   private BlockingDeque runableDeque;
   public MyExcutorTest(int maxThreadCount, int dequeSize) {
       runableDeque = new LinkedBlockingDeque<Runnable>(dequeSize);
       workList = new ArrayList<WorkThread>(maxThreadCount);
        for (int i=0;i<maxThreadCount;i++) {
            new Thread(new WorkThread(),"MY-EWB").start();
        }
   }

   public Boolean execute(Runnable runnable) {
       return runableDeque.offer(runnable);
   }

   class WorkThread implements Runnable {
       @Override
       public void run() {
           while (true) {
//               System.out.println("run start....." + Thread.currentThread().getName()+ " " + Thread.currentThread().getId());
               try {
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               Runnable command = (Runnable) runableDeque.poll();
               if(command != null) {
                   command.run();
               }
           }
       }
   }

    public static void main(String[] args) {
        MyExcutorTest executorService = new MyExcutorTest(2,2);
        for (int i=0;i<10;i++) {
            final int finalI = i;
            Boolean result = executorService.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName()+","+finalI);
                }
            });
            System.out.println(result + " 第******************************" + finalI );
        }
    }
}
