package com.ding.learn.test.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Stu {
    private String name;
    private boolean needKo;
}
