package com.ding.learn.test.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.Objects;

@Data
@Accessors(chain = true)
public class Student {
	private Integer id;
	private String name;
	private String sex;
	private String country;
	@JsonProperty
	private String kCode;
	private Integer age;
	private int intage;
	private Date createTime;
	private Boolean flag;
	

	public Student() {
		super();
	}
	public Student(Integer id, String name, String sex) {
		super();
		this.id = id;
		this.name = name;
		this.sex = sex;
	}
	/*@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", sex=" + sex + "]";
	}*/
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj == null) {
			return false;
		}
		if(getClass() != obj.getClass())
			return false;
		Student student = (Student) obj;
        return Objects.equals(name, student.name) &&
                Objects.equals(sex, student.sex);
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	public static void main(String[] args) {
		/*Student stu = new Student();
		if(stu.getId() != null && stu.getId() == 1) {
			System.out.println(111);
		} else {
			System.out.println(222);
		}*/
		Student stu = new Student();
		stu.name = "";
		System.out.println(stu);
	}
	
}
