package com.ding.learn.test.date;

import com.beust.jcommander.internal.Lists;
import com.ding.learn.test.dto.Student;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class TestDateSHiqu {
    public static void main(String[] args) throws ParseException {
        test4();
    }

    private static void test1() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date = new Date();
        System.out.println(date);
        String str = format.format(date);
        System.out.println(str);
        System.out.println(TimeZone.getTimeZone(ZoneId.of("UTC+8")));
    }

    private static void test2() throws ParseException {
        //1、普通转换
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateStr = sdf.format(date);
        System.out.println(dateStr);

        String dateStr1 = "2021-12-10 13:40:44";
        Date date1 = sdf.parse(dateStr1);
        System.out.println(date1.getTime());

        long timeMill = 1639115158;
        String myDate = "2021-12-10 20:45:58";
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date2 = sdf1.parse(myDate);
        System.out.println(date2);

        SimpleDateFormat targetSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        targetSdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        String checkpoint_date = "2021-12-10 20:45:58";
        SimpleDateFormat sourceSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String status_date = targetSdf.format(sourceSdf.parse(checkpoint_date));
        System.out.println(status_date);
    }

    public static void test3() {
        /**
         * 您使用的是错误的模式，H是一天中的小时(0-23)；您需要h.
         * 错误告诉您H是 24小时小时，因此8显然是上午.因此，当您告诉解析器在24小时时钟上使用8并告诉解析器它是PM时，它会炸毁
         */
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("M/d/yyyy h:m a",Locale.ENGLISH);
//        LocalDateTime localDateTime =  LocalDateTime.now();
        LocalDateTime localDateTime =  LocalDateTime.parse("12/01/2021 02:21 PM",dtf);
        System.out.println(localDateTime.toInstant(ZoneOffset.UTC).toString());
        System.out.println(localDateTime);

        Date dsd = new Date();
        LocalDateTime date1 = LocalDateTime.ofInstant(Instant.ofEpochMilli(new Date().getTime()),ZoneId.of("GMT+06:00"));
        System.out.println(date1);

        Instant instant = Instant.ofEpochMilli(new Date().getTime());
        System.out.println(instant);

        System.out.println(LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli());
        System.out.println(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
    }

    public static void test4() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'+08:00'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date actualDate = null;
        try {
            actualDate = sdf.parse("2021-12-10T15:16:53+08:00");
            System.out.println((actualDate.getTime() / 1000));
            System.out.println(actualDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static void test5() {
        List<Student> list = Lists.newArrayList();
        list.add(null);
        list.stream().forEach(n->{
            System.out.println(Optional.ofNullable(n).orElse(new Student()));
        });
        System.out.println("正常代码");
        System.out.println("test git");
//        if(null != list.get(0)) {
//            System.out.println("11111111111");
//        }
    }

}
