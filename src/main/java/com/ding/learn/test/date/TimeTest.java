package com.ding.learn.test.date;

import com.alibaba.fastjson.JSONObject;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class TimeTest {
    public static void main(String[] args) {
        LocalDate startDate = LocalDate.parse("2021-01-16",DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate = LocalDate.parse("2021-02-17");
        System.out.println(startDate.until(endDate).getDays());
        long days = startDate.until(endDate, ChronoUnit.DAYS);
        System.out.println(startDate.until(endDate, ChronoUnit.DAYS));
        if(days<0) {
            System.out.println("============");
        }
    }

    public static void main1(String[] args) {
        JSONObject obj = new JSONObject();
        BigDecimal b = new BigDecimal("17.8");
        System.out.println(b);
    }
}
