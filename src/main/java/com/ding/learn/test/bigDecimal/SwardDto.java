package com.ding.learn.test.bigDecimal;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SwardDto {
    //账单金额
    private BigDecimal totalPayMoney;
    //抵扣金额
    private BigDecimal deductionMoney;
    //实际付款金额
    private BigDecimal payrealMoney;


    public static void main1(String[] args) {
        JSONObject obj = new JSONObject();
        obj.put("totalPayMoney","15.65");
        obj.put("deductionMoney",14.52);
        obj.put("payrealMoney",3.1);
        SwardDto dto = JSONObject.parseObject(obj.toJSONString(),SwardDto.class);
        System.out.println(dto.getTotalPayMoney().multiply(new BigDecimal(10)));
        System.out.println(dto);
    }

    public static void main(String[] args) {
        System.out.println(new BigDecimal(0.01).subtract(new BigDecimal(0.01)));
        System.out.println(new BigDecimal("0.01").subtract(new BigDecimal("0.01")));
    }
}
