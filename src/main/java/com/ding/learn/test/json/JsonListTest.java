package com.ding.learn.test.json;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.stream.Collectors;

public class JsonListTest {
    public static void main(String[] args) {
        List<JSONObject> list = Lists.newArrayList();
        JSONObject o1 = new JSONObject() {{
            put("name", "张三");
            put("sex", "nan");
            put("age", 123);
        }};
        JSONObject o2 = new JSONObject() {{
            put("name", "张三");
            put("sex", "nan");
            put("age", 1234);
        }};
        JSONObject o3 = new JSONObject() {{
            put("name", "张三");
            put("sex", "nan");
            put("age", 123);
        }};
        list.add(o1);
        list.add(o2);
        list.add(o3);
        list.stream().distinct().forEach(n-> System.out.println(n));

    }
}
