package com.ding.learn.servie.impl;

import com.ding.learn.servie.IADealService;
import org.springframework.stereotype.Service;

@Service
public class ADealServiceImpl extends AbstractDealServiceImpl implements IADealService {
    @Override
    public void dealDataOrder() {
        dealData();
    }

    @Override
    public void doProcessOrderToDB() {
        System.out.println("A Class deal");
    }
}
