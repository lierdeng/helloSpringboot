package com.ding.learn.servie.impl;

import com.ding.learn.servie.IBDealService;
import org.springframework.stereotype.Service;

@Service
public class BDealServiceImpl extends AbstractDealServiceImpl implements IBDealService {

    @Override
    public void dealDataOrder() {
        dealData();
    }

    @Override
    public void doProcessOrderToDB() {
        System.out.println("B Class deal");
    }
}
