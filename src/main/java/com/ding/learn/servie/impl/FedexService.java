package com.ding.learn.servie.impl;

import com.alibaba.fastjson.JSONObject;
import okhttp3.*;

import java.io.IOException;

public class FedexService {

    public static void main1(String[] args) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
// 'input' refers to JSON Payload

        JSONObject json = new JSONObject();
        json.put("grant_type", "client_credentials");
        json.put("client_id", "l7f9fb563c94434cb4829007beff115dcb");
        json.put("client_secret", "adcfdea186024e2981f167b27ac92a4c");
        RequestBody body = RequestBody.create(mediaType, json.toJSONString());
        Request request = new Request.Builder()
                .url("https://apis-sandbox.fedex.com/oauth/token")
                .post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();

        Response response = client.newCall(request).execute();
        System.out.println("123");
    }

    public static void main(String[] args) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "grant_type=client_credentials&client_id=l7f9fb563c94434cb4829007beff115dcb&client_secret=adcfdea186024e2981f167b27ac92a4c");
        Request request = new Request.Builder()
                .url("https://apis-sandbox.fedex.com/oauth/token")
                .method("POST", body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();
        Response response = client.newCall(request).execute();
        System.out.println(response.body().string());
    }
}
