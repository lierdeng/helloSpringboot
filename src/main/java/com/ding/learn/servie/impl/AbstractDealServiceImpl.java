package com.ding.learn.servie.impl;

import com.ding.learn.servie.IAbstractDealService;

public abstract class AbstractDealServiceImpl{
    protected String name = "3434";

    public void dealData() {
        System.out.println("这里编写公共的方法");
        doProcessOrderToDB();
    }

    protected abstract void doProcessOrderToDB();
}
