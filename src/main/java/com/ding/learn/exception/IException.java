package com.ding.learn.exception;

public interface IException {
    /**
     * 获取code
     * @return String
     */
    String getCode();

    String getMessage();

    default Object getData() { return  null; }

    /**
     * 获取异常信息
     * @return String
     */
    String getNativeMessage();

    /**
     * 设置异常参数
     * @param objects 异常参数
     */
    void setArguments(Object... objects);

    /**
     * 获取异常参数
     * @return 异常参数
     */
    Object[] getArguments();
}
