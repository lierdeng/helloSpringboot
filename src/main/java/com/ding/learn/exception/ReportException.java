package com.ding.learn.exception;

public class ReportException extends BusinessException {

    public ReportException(String message) {
        super(message);
    }

    public ReportException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReportException(String code, String message) {
        super(code, message);
        System.out.println(super.getMessage());
    }

    public ReportException(String code, String message, Throwable cause) {
        super(code, message, cause);
    }

}
