package com.ding.learn.exception;

public class BusinessException extends RuntimeException implements IException {

    /**
     * 异常code
     */
    protected String code;

    protected Object data;

    /**
     * 异常信息
     */
    private String nativeMessage;

    /**
     * 异常 arguments
     */
    private Object[] arguments;


    public BusinessException() {
        super();
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(String code, String message) {
        super(message);
        this.code = code;
    }

    public BusinessException(String code, Object data) {
        super();
        this.code = code;
        this.data = data;
    }

    public BusinessException(String code, String message, Object data) {
        super(message);
        this.code = code;
        this.data = data;
    }

    public BusinessException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public BusinessException(String code, String message, String nativeMessage) {
        super(message);
        this.code = code;
        this.nativeMessage = nativeMessage;
    }

    public BusinessException(String code, String message, String nativeMessage, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.nativeMessage = nativeMessage;
    }

    public BusinessException(String code, Object... arguments) {
        super();
        this.code = code;
        this.arguments = arguments;
    }

    public BusinessException(String code, String msg, Object... arguments) {
        super(msg);
        this.code = code;
        this.arguments = arguments;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNativeMessage() {
        return this.nativeMessage;
    }

    @Override
    public void setArguments(Object... objects) {
        this.arguments = arguments;
    }

    @Override
    public Object[] getArguments() {
        return this.arguments;
    }

    @Override
    public Object getData() {
        return this.data;
    }

    @Override
    public String getMessage() {
        return this.nativeMessage == null ? super.getMessage() : this.nativeMessage;
    }
}
