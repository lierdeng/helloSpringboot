package com.ding.learn.controller;

import com.ding.learn.servie.IADealService;
import com.ding.learn.servie.impl.AbstractDealServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/luban")
public class TestExtendsController {

    @Autowired
    private IADealService aDealService;

    @RequestMapping("/dealData")
    public void dealData() {
        System.out.println(aDealService);
        aDealService.dealDataOrder();
    }
}
