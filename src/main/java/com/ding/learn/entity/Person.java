package com.ding.learn.entity;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class Person {
    private String name;
    private String sex;

    public static void main(String[] args) {
        Person p = Person.builder().name("dsds").build();
        Person p1 = new Person("dsd","dsd");
        System.out.println(p);
        System.out.println(p1);
        p.name = "";
    }
}


